package pl.silesiakursy.grade.model;

public enum GradeEnum {
        NIEDOSTATECZNA,
        DOPUSZCZAJACA,
        DOSTATECZNA,
        DOBRA,
        BARDZO_DOBRA,
        CELUJACA;
}
