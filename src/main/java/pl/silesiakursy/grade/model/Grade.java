package pl.silesiakursy.grade.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Grade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "TEST_POINT")
    private int numberPointsFromTest;
    @Enumerated(EnumType.STRING)
    private GradeEnum gradeOfTest;
    private Date testDate;
    @ManyToOne
    @JoinColumn(name="STUDENT_ID", nullable=false)
    private Student student;
    @ManyToOne
    @JoinColumn(name="SUBJECT_ID", nullable=false)
    private Subject subject;

    public Grade() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberPointsFromTest() {
        return numberPointsFromTest;
    }

    public void setNumberPointsFromTest(int numberPointsFromTest) {
        this.numberPointsFromTest = numberPointsFromTest;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public GradeEnum getGradeOfTest() {
        return gradeOfTest;
    }

    public void setGradeOfTest(GradeEnum gradeOfTest) {
        this.gradeOfTest = gradeOfTest;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
