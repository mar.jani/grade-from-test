package pl.silesiakursy.grade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.silesiakursy.grade.service.GradeService;

@Controller
public class GradeController {
    private GradeService gradeService;

    @Autowired
    public GradeController(GradeService gradeService) {
        this.gradeService = gradeService;
    }

    @GetMapping("/grades")
    public String getGrade() {
        return "index";
    }

    @GetMapping("/grades/results")
    public String getGrade(@RequestParam("testResult") int testResult, Model model) {
        if(testResult >=0){
            model.addAttribute("testResult", gradeService.getGrade(testResult));
        }
        return "index" ;
    }
}
