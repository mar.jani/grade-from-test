package pl.silesiakursy.grade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.silesiakursy.grade.model.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
}
