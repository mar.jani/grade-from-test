package pl.silesiakursy.grade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.silesiakursy.grade.service.Grade;

public interface GradeRepository extends JpaRepository<Grade, Long> {
}
