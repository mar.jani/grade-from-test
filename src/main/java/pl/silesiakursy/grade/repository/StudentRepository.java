package pl.silesiakursy.grade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.silesiakursy.grade.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
