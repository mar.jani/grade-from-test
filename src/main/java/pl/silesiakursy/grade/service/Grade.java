package pl.silesiakursy.grade.service;

public interface Grade {
    String getGrade(int number);
}
