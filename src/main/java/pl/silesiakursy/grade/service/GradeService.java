package pl.silesiakursy.grade.service;

import org.springframework.stereotype.Service;
import pl.silesiakursy.grade.model.GradeEnum;

import static pl.silesiakursy.grade.model.GradeEnum.*;


public class GradeService implements Grade {
    public String getGrade(int testResult) {
        if (testResult < 0) {
            return "Ilość punktów jest mniejsza niż 0";
        } else if (testResult > 100) {
            return "Ilość punktów jest większa niż 100";
        }
        return calculateGrade(testResult).toString();
    }

    public String getGrade() {
        return "Podaj wynik testu";
    }

    private GradeEnum calculateGrade(int points) {
        GradeEnum grade = NIEDOSTATECZNA;
        if (points >= 40 && points <= 54) {
            grade = DOPUSZCZAJACA;
        } else if (points >= 55 && points <= 69) {
            grade = DOSTATECZNA;
        } else if (points >= 70 && points <= 84) {
            grade = DOBRA;
        } else if (points >= 85 && points <= 98) {
            grade = BARDZO_DOBRA;
        } else if (points >= 99 && points <= 100) {
            grade = CELUJACA;
        }
        return grade;
    }
}