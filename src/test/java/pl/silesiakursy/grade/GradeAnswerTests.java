package pl.silesiakursy.grade;

//import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import pl.silesiakursy.grade.service.GradeService;

public class GradeAnswerTests {
    GradeService gradeService = new GradeService();

    @Test
    public void shouldReturnGradeF(){
        //given
        String result = gradeService.getGrade(10);

        //when
        String testString = "OCENA:  niedostateczna";

        //then
        Assertions.assertEquals(testString,result);
    }

    @Test
    public void shouldReturnGradeD(){
        //given
        String result = gradeService.getGrade(69);

        //when
        String testString = "OCENA:  dostateczna";

        //then
        Assertions.assertEquals(testString,result);
    }

    @Test
    public void shouldReturnGradeA(){
        //given
        String result = gradeService.getGrade(99);

        //when
        String testString = "OCENA:  celująca";

        //then
        Assertions.assertEquals(testString,result);
    }

}
